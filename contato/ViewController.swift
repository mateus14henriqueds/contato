//
//  ViewController.swift
//  contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome :String
    let email :String
    let numero :String
    let endereco :String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContato:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContato.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContato[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.email.text = contato.email
        cell.numero.text = contato.numero
        cell.endereco.text = contato.endereco
        
        return cell
    }
    

    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        listaDeContato.append(Contato(nome: "Rodrigo", email: "rodrigo@gmail.com", numero: " 31 9 95483-0293", endereco: "Rua Cristal 11"))
        listaDeContato.append(Contato(nome: "Mateus", email: "mateus@gmail.com", numero: " 31 9 8676-7543", endereco: "Rua Cristal 31"))
        listaDeContato.append(Contato(nome: "Ana", email: "ana@gmail.com", numero: " 31 9 2367-9654", endereco: "Rua Cristal 263"))
        listaDeContato.append(Contato(nome: "Rafaela", email: "rafaela@gmail.com", numero: " 31 9 9647-2346", endereco: "Rua Cristal 131"))
        
        
    }


}

